#!/bin/sh

if [ "$SQL_ENGINE" = "django.db.backends.postgresql" ]
then
    echo "Waiting for postgres..."

    while ! nc -z $SQL_HOST $SQL_PORT; do
      sleep 0.1
    done

    echo "PostgreSQL started"
fi

echo "Waiting for mail server..."

while ! nc -z $EMAIL_HOST $EMAIL_PORT; do
  sleep 0.1
done

echo "Mail server started"

exec "$@"
